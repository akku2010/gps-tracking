import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
import { ViewController, NavParams, ToastController, AlertController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
    selector: 'page-update-cust',
    templateUrl: './update-cust.html'
})

export class UpdateCustModalPage implements OnInit {
    getAllDealersData: any;
    dealerdata: any;
    isSuperAdminStatus: boolean;
    updatecustForm: FormGroup;
    islogin: any;
    submitAttempt: boolean;
    devicedetail: any = {};
    customer: any;
    vehType: any;
    editdata: any;
    yearLater: any;
    minDate: any;
    customerdata: any = {};

    constructor(
        public apiCall: ApiServiceProvider,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        navPar: NavParams,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        private translate: TranslateService
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        this.customer = navPar.get("param");

        if (this.customer.expiration_date == null) {
            var tru = moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            tempdate.setDate(tempdate.getDate() + 365);
            this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        } else {
            this.yearLater = moment(new Date(this.customer.expiration_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
        }

        // ============== one month later date from current date ================
        debugger
        var today: any = new Date();
        var dd: any = today.getDate();
        var mm: any = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        // var tdate = new Date();
        // var tempmin = tdate.setMonth(tdate.getMonth());
        // this.minDate = moment(new Date(today), 'DD-MM-YYYY').format("YYYY-MM-DD");
        this.minDate = today;
        console.log("minimum date: ", today);
        // =============== end
        this.updatecustForm = formBuilder.group({
            userid: [this.customer.userid, Validators.required],
            first_name: [this.customer.first_name, Validators.required],
            last_name: [this.customer.last_name, Validators.required],
            email: [this.customer.email],
            phone: [this.customer.phone],
            address: [this.customer.address],
            creationdate: [moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD')],
            expirationdate: [this.yearLater],
            dealer_firstname: [this.customer.dealer_firstname],
        })

        // this.updatecustForm.patchValue()
    }

    ngOnInit() {
        this.getAllDealers();
    }

    // getAllDealers() {
    //     var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles';
    //     this.apiCall.getAllDealerVehiclesCall(baseURLp)
    //         .subscribe(data => {
    //             this.getAllDealersData = data;
    //         }, err => {
    //             console.log(err);
    //         });
    // }

    getAllDealers() {
        var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        let toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apiCall.getAllDealerCall(baseURLp)
            .subscribe(data => {
                this.getAllDealersData = data;
                // toast.dismiss();
            },
                error => {
                    console.log(error)
                });
    }

    DealerselectData(dealerselect) {
        this.dealerdata = dealerselect;
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }


    updateCustomer() {
        this.submitAttempt = true;
        if (this.updatecustForm.valid) {
            this.devicedetail = {
                "contactid": this.customer._id,
                "address": this.updatecustForm.value.address,
                "expire_date": new Date(this.updatecustForm.value.expirationdate).toISOString(),
                "first_name": this.updatecustForm.value.first_name,
                "last_name": this.updatecustForm.value.last_name,
                "status": this.customer.status,
                "user_id": this.updatecustForm.value.userid,
                "email": this.updatecustForm.value.email,
                "phone": this.updatecustForm.value.phone
            }
            if (this.dealerdata != undefined) {
                this.devicedetail.Dealer = this.dealerdata.dealer_id;
            }
            // else {
            //     this.devicedetail.Dealer = this.islogin._id;
            // }

            if (this.vehType == undefined) {
                this.devicedetail;
            } else {
                this.devicedetail.vehicleType = this.vehType._id;
            }

            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(this.devicedetail)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.editdata = data;
                    let toast = this.toastCtrl.create({
                        message: this.translate.instant('dealerUpdated', { value: this.translate.instant("Customers") }),
                        position: 'bottom',
                        duration: 2000
                    });

                    toast.onDidDismiss(() => {
                        this.viewCtrl.dismiss(this.editdata);
                    });

                    toast.present();
                }, err => {
                    this.apiCall.stopLoading();
                    var body = err._body;
                    var msg = JSON.parse(body);
                    let alert = this.alerCtrl.create({
                        message: msg.message,
                        buttons: [this.translate.instant('Okay')]
                    });
                    alert.present();
                });
        }

    }
}
