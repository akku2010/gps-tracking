import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaintenanceReminderPage } from './maintenance-reminder';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MaintenanceReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(MaintenanceReminderPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class MaintenanceReminderPageModule {}
